<?php

namespace Dd8Starter;

use Composer\Script\Event;
use Composer\IO\IOInterface;

class ScriptHandler
{

  /**
   * @param Event $event
   */
  public static function printRootPackageMessage(Event $event)
  {
    /** @var  $io */
    $io = $event->getIO();
    $io->write("<bg=blue;fg=white>DD8 Starter has been installed. Let's now configure your local environnement.</>");

    // ask for project name
    $projectName = $io->ask("Please, enter your project name :", "dd8_starter_project_default_name");

    // ask for project local URL
    $projectUrl = $io->ask("Please, enter your project local URL :", "dd8-starter-project-default-name.localhost");

    // edit .env file to place user's values
    $file='.env';

    //read file
    $content=file_get_contents($file);

    // here goes the update
    $content = preg_replace('/PROJECT_NAME=dd8starter/', 'PROJECT_NAME='.$projectName, $content);
    $content = preg_replace('/PROJECT_BASE_URL=dd8starter.localhost/', 'PROJECT_BASE_URL='.$projectUrl, $content);

    // write file
    file_put_contents($file, $content);
  }

  /**
   * @param Event $event
   */
  public static function printCreateProjectMessage(Event $event)
  {
    $event->getIO()->write('<bg=blue;fg=white>DD8 starter and dependencies are now installed. Thank you for using DD8 Starter</>');
  }
}
