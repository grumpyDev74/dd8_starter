#!/usr/bin/env bash

echo "Start docker containers"
docker-compose up -d

echo "Install coomposer dependencies"
docker-compose exec -T php sh -c "composer install"
