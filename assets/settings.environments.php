<?php

assert_options(ASSERT_ACTIVE, TRUE);
\Drupal\Component\Assertion\Handle::register();

// check ACQUIA environment variable
defined('AH_SITE_ENVIRONMENT')
|| define('AH_SITE_ENVIRONMENT', (getenv('AH_SITE_ENVIRONMENT') ?
  getenv('AH_SITE_ENVIRONMENT') :
  'dev'));

// include environment settings file
if (file_exists(DRUPAL_ROOT . '/sites/default/settings/'.AH_SITE_ENVIRONMENT.'settings.php')) {
  include DRUPAL_ROOT . '/sites/default/settings/'.AH_SITE_ENVIRONMENT.'settings.php';
}
