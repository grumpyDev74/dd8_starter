﻿# DD8 - DRUPAL 9 STARTER

To create a new project just run the following command :

```
composer create-project grumpydev74/dd8_starter:^9.0 {your-destination-folder-there} --repository-url='{"type": "vcs","url": "https://gitlab.com/grumpyDev74/dd8_starter.git"}' --remove-vcs --no-install
```

After cloning the project you will be prompted to enter docker4drupal project name and project local host domain
